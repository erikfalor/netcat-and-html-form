#!/bin/bash
PORT=8000
N=0;

NETCAT=nc
#NETCAT=ncat
#NETCAT=netcat


while sleep .25; do
	echo
	echo Listening on http://localhost:$PORT
	echo "[Hit Ctrl-C once to reload the document on the server]"
	echo "[Hit Ctrl-C twice in a row to exit]"
	echo "======================================================"
	(( N++ ))
	cat headers.http index.html | $NETCAT -l -p $PORT -q 1 | pygmentize -l http 2>/dev/null
    echo "Request #$N received @ $(date)"
	echo
	sleep .25
done
